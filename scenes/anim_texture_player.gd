
extends AnimatedSprite

var dir_util = preload("res://util/dir.gd")
onready var queue = get_node("/root/resource_queue")

export(float) var speed = 20
export(NodePath) var progress
var anim_name = "default"

var files = []
var offset = Vector2(0,0)
var scale = Vector2(1,1)
var amt_to_offset = 5

func _unhandled_input(event):
	if(event.is_action_pressed("speed_up")):
		speed += 1
		set_speed(speed)
	if(event.is_action_pressed("speed_down")):
		speed -= 1
		set_speed(speed)
	if(event.is_action_pressed("scale_up")):
		scale.x += 0.1
		scale.y += 0.1
		on_resize()
	if(event.is_action_pressed("scale_down")):
		scale.y -= 0.1
		scale.x -= 0.1
		on_resize()
	if(event.is_action_pressed("move_up")):
		offset += Vector2(0,-amt_to_offset)
		on_resize()
	if(event.is_action_pressed("move_down")):
		offset += Vector2(0,amt_to_offset)
		on_resize()
	if(event.is_action_pressed("move_left")):
		offset += Vector2(-amt_to_offset,0)
		on_resize()
	if(event.is_action_pressed("move_right")):
		offset += Vector2(amt_to_offset,0)
		on_resize()

func set_speed(s):
	var frames = get_sprite_frames()
	frames.set_animation_speed(anim_name, s)
	set_sprite_frames(frames)

func _ready():
	get_tree().connect("screen_resized", self, "on_resize")
	set_process_unhandled_input(true)

func on_resize():
	var v = get_viewport()
	var tex = get_sprite_frames().get_frame("default", 0)

	if(tex != null):
		var img = tex.get_data()
		var imgrect = img.get_used_rect()
		var rect = v.get_visible_rect()

		var center_screen = Vector2(rect.size.width/2, rect.size.height/2)

		set_pos(center_screen + offset)
		set_scale(Vector2((rect.size.width / imgrect.size.width) * scale.x, (rect.size.height / imgrect.size.height) * scale.y))

	pass

func load_folder(path):
	files = dir_util.recurse_dir(path)

	var frames = SpriteFrames.new()
	frames.set_animation_speed(anim_name, speed)
	set_sprite_frames(frames)
	play(anim_name)

	queue_shit()

func queue_shit():
	for x in files:
		queue.queue_resource(x)

	if(files.size() > 0):
		get_node("CanvasLayer/ProgressBar").show()
		set_process(true)

func update_progress(f):
	get_node("CanvasLayer/ProgressBar").set_value(f)

func finish_loading():
	get_node("CanvasLayer/ProgressBar").hide()
	#set_process(false)
	play(anim_name)

func _process(delta):
	if(files.size() > 0):
		if(queue.is_ready(files[0])):
			var tex = queue.get_resource(files[0])
			tex.fix_alpha_edges()

			var frames = get_sprite_frames()
			frames.add_frame(anim_name, tex)
			set_sprite_frames(frames)
			files.pop_front()

			on_resize()

		else: #current file not ready
			update_progress(queue.get_progress(files[0]))
	else:
		finish_loading()