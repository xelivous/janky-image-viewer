
extends Node

var files

onready var image_node = get_node("View/Image")
onready var video_node = get_node("View/Video")
onready var anim_node = get_node("View/AnimatedSprite")

func _ready():
	files = {}
	get_tree().connect("files_dropped", self, "on_drop")

func is_file(path):
	return path.extension() != path

func on_drop(f, screen):
	get_node("BG/MarginContainer").hide()

	for x in f:
		if(is_file(x)):
			var tex = load(x)
			tex.fix_alpha_edges()
			image_node.set_texture(tex)
		else:
			anim_node.load_folder(x)


