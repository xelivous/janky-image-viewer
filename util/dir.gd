
#obj should be an array
static func recurse_dir(path, obj = []):
	var dir = Directory.new()
	dir.open(path)

	var files = obj

	if dir.open(path) == OK:
		dir.list_dir_begin()

		var file_name = dir.get_next()

		while (file_name != ""):
			var actual_file = path.plus_file(file_name)

			if (file_name != "." and file_name != ".."):
				if dir.current_is_dir():
					recurse_dir(actual_file, files)
				else:
					files.append(actual_file)

			file_name = dir.get_next()
	else:
		printt(path, "An error occurred when trying to access the path.")

	return files

